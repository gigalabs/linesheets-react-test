import React, { useState, useEffect } from 'react'
import arrayMove from 'array-move'
import { Images } from '../assets'
import { SortableList } from '../components'

const Screen = () => {
  const [images, setImages] = useState([])

  useEffect(() => {
    setImages(Images)
  }, [])

  const onSortEnd = ({ oldIndex, newIndex }) =>
    setImages(arrayMove(images, oldIndex, newIndex))

  return (
    <SortableList
      shouldUseDragHandle={true}
      useDragHandle
      axis='xy'
      items={images}
      onSortEnd={onSortEnd}
    />
  )
}

export default Screen
