import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 3%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
`

export const StyledItem = styled.div`
  flex-basis: 15%;
  max-width: 15%;
  padding: 5px;
  @media all and (max-width: 1024px) {
    flex-basis: 23%;
    max-width: 23%;
  }
  @media all and (max-width: 600px) {
    flex-basis: 45%;
    max-width: 45%;
  }
`

export const HandleStyle = styled.div`
  cursor: grab;
`

export const ImageContainer = styled.img``
