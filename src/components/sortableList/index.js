import React from 'react'
import AspectRatio from 'react-aspect-ratio'
import {
  SortableContainer,
  SortableElement,
  SortableHandle
} from 'react-sortable-hoc'
import { Container, StyledItem, HandleStyle, ImageContainer } from './style'
import 'react-aspect-ratio/aspect-ratio.css'

const Handle = SortableHandle(({ image }) => (
  <HandleStyle>
    <AspectRatio ratio='16/9'>
      <ImageContainer src={image} />
    </AspectRatio>
  </HandleStyle>
))

const SortableItem = SortableElement(props => (
  <StyledItem>
    {props.shouldUseDragHandle && <Handle image={props.value} />}
  </StyledItem>
))

const SortableList = SortableContainer(props => {
  const { items, ...restProps } = props
  return (
    <Container>
      {items.map((item, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          value={item}
          {...restProps}
        />
      ))}
    </Container>
  )
})

export default SortableList
